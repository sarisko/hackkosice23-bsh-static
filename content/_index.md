## B/S/H challenge documentation

This website documents our progress with the B/S/H challenges during the 1.4-2.4 2023 [Hack Kosice](https://hackkosice.com/hackathon/).

We managed to complete challenge 1 and 2, and partially challenge 3 and 6. We were unfortunately stuck on figuring out the inertial measurement unit, so we didn't manage to complete the serial connection between the board and our application.

{{< figure src="team_pic.jpg" caption="The culprits" height="450">}}
