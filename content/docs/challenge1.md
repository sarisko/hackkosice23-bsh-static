---
linktitle: Challenge 1
next: /challenge2
title: Smooth Color Transition of the Whole Matrix of LEDs
weight: 10
---


## Problem description

- Ensure that all LEDs have the same color
- Use the entire 16-bit color spectrum
- Each color cycle should last a maximum of 5 seconds
- The color transition across all LEDs should be smooth

## Implementation

{{< youtube BaFBy6F0Bm8 >}}

