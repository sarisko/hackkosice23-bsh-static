---
linktitle: Challenge 6
prev: /challenge5
title: Mobile Phone Application Control
weight: 10
---

## Problem description

- Create a mobile phone application to control the matrix of LEDs and replace the motion sensor board with the sensorics from the mobile phone. Ensure that the motion sensor board is switched off remotely by the application

## Implementation

The application runs locally (using HTTPS, which is necessary to be able to securely send the sensitive data) and reads device orientation data (from phone). These are sent as alpha, beta and gamma angles to the application backend and saved in a file - this file was intended to be later accessed by the STM board.

We decided to use a web interface instead of a native app, because it is accessible by the wider range of phones.

Android phone:
{{< youtube zirQ-02hK8o >}}

iPhone:
{{< youtube -TWH5N9EqxQ >}}