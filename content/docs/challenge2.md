---
linktitle: Challenge 2
next: /challenge3
prev: /challenge1
title: Smooth Color Transition of Every Single Row of LEDs
weight: 10
---

## Problem description

- Ensure that all LEDs in a row have the same color
- Create a gradual "falling" effect of colored rows from the top to thebottom of the matrix of LEDs.
- The animated 'b' 's' 'h' symbol should also gradually "fall" from the top to the bottom.
- Each color cycle should last a maximum of 5 seconds.
- The color transition across each row of LEDs should be smooth

## Implementation

First part:
{{< youtube pKtY5YNCEfM >}}

Second part:
{{< youtube d0P3WaHfiiw >}}