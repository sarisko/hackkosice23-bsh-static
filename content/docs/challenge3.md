---
linktitle: Challenge 3
next: /challenge4
prev: /challenge6
title: Motion Sensor Controlled Animation
weight: 10
---

## Problem description

- Use the hardware motion sensor to create a simple animation where the active row (the one that changes color) changes depending on the tilt angle of the motion sensor.
- When the sensor is tilted to one side, the active row should gradually "fall" in the same direction.
- The same "falling" effect should apply to the animated 'b' 's' 'h' symbol.
- The horizontal position of the motion sensor should stop the movement of the active row.
- Each color cycle should last a maximum of 5 seconds

## Implementation

{{< youtube aeiOYvq5jjw >}}
The demo - not complete solution.